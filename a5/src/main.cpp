/*
This program asks you to implement the K-Means Clustering algorithm in C++. The program will read in data from a comma 
separated value file and use it to identify clusters in the data.
Create a class called KMeansCluster that has the following public interface:

KMeansCluster(vector<vector<double>>& kmd);
unordered_map<string, vector<vector<double>>> createClusters(int numClusters, int attempts);

The constructor takes in a two-dimensional collection of double data. This data will be stored in the KMeansCluster object 
to be used by one or more helper methods. 

The method createClusters() takes in a requested number of clusters and will generate the best cluster centers given the data 
passed in to the constructor. By best, I mean that the program will make some random attempts at finding the cluster centers 
with the minimum distance from all the data points to the chosen cluster centers. Once you find the best cluster centers out 
of the requested number of attempts you will print each cluster center and all of the data points associated with it. 

The return type for the createClusters() method is an unordered_map. Some other names for this type is are hash table, 
dictionary, and associative array. A hash table is a container that stores values based on key/value pairs. The key is used to 
retrieve a value. Please look at this playback for some example code 
(https://ourcodestories.com/markm208/Playlist/15/Playback/378).

I suggest creating unordered_map's for the cluster centers, and a mapping between a data point and its cluster center.

Here is some code to that I may use when grading:

KMeansCluster kmc(petalData);
unordered_map<string, vector<vector<double>>> minimumClusterCenter = kmc.createClusters(2, 100);

for (auto it = minimumClusterCenter.begin(); it != minimumClusterCenter.end(); ++it){
    pair<string, vector<vector<double>>> clusterCenter = *it;

	string clusterCenterString = clusterCenter.first;
	cout << "Cluster Center: " << clusterCenterString << endl;

	vector<vector<double>> clusterData = clusterCenter.second;

	for (int i = 0; i < clusterData.size(); i++){
		for (int j = 0; j < clusterData[i].size(); j++)
			cout << clusterData[i][j] << " ";
		cout << endl;
	}
}

*/
#include <iostream>
#include <fstream>

#include "KMeansCluster.hpp"

using namespace std;

vector<vector<double>> readDataFile(string filename){
	ifstream file(filename);
	if(!file.is_open()){
		string e = "Could not open the file: " + filename;
		throw e;
	}

	vector<vector<double>> data;
	//1st 2 columns are useless data, 2nd 2 columns are what we want
	string line;

	while(getline(file, line)){
		//cout<<line;
		line = line.substr(line.find(',')+1);
		//cout<<" "<<line;
		line = line.substr(line.find(',')+1);
		//cout<<" "<<line;
		double d1 = stod(line.substr(0,line.find(',')+1));
		double d2 = stod(line.substr(line.find(',')+1));
		data.push_back(vector<double>{d1,d2});
		
	}

	return data;
}

int main(){
	//string filename = "/home/abiewer/Documents/ai-class/a5/iris-versicolor-setosa-no-labels.txt";
	string filename = "/home/abiewer/Documents/ai-class/a5/smallTestData.txt";

	//DO NOT RUN THIS AS IS, YOU NEED TO MAKE CHANGES TO THE ALGORITHM AT THE END OF THE KMEANSCLUSTER FILE

	//string filename = "/mnt/c/Users/Adam/Desktop/ai-class/a5/smallTestData.txt";

	try{
		//throws for bad file name
		auto data = readDataFile(filename);

		KMeansCluster kmc(data);
		kmc.printData();

		//throws for bad inputs
		auto results = kmc.createClusters(2, 1000);

		//debugging
		cout<<"Cluster Centers created:"<<endl;
		for(auto iter = results.begin();iter!=results.end();iter++){
			cout<<(*iter).first<<endl;
		}
		cout<<endl;
	}
	catch(string e){
		cout<<e<<endl;
		return 1;
	}

	return 0;
}