#include "KMeansCluster.hpp"
#include <float.h>

using namespace std;

/**
 * Creates a KMC for the data given
 * 
 * @kmd the data
 * */
KMeansCluster::KMeansCluster(vector<vector<double>> &kmd){
    data = kmd;
    removeDuplicates();
}

void KMeansCluster::removeDuplicates(){
    vector<size_t> r; //removeable elements

    //for each point in the data
    for (size_t i = 0; i < data.size(); i++){
        //for each point after the current point
        for (size_t j = i + 1; j < data.size(); j++){
            bool same = true;
            //check each value to see if any are different
            for (size_t k = 0; k < data[i].size(); k++){
                if (data[i][k] != data[j][k]){
                    //and break if they are different
                    same = false;
                    break;
                }
            }
            //if the points are the same, we need to delete them
            if (same)
                r.push_back(j);
        }
    }

    //get rid of duplicate indecies
    sort(r.begin(), r.end());
    r.erase(unique(r.begin(), r.end()), r.end());

    //make erasing possibly faster by working back to front
    reverse(r.begin(), r.end());

    //erase all the duplicate points
    for (auto iter = r.begin(); iter != r.end(); iter++)
    {
        data.erase(data.begin() + *iter);
    }
}

/**
 * Using the data points of the class, it will generate 'numClusters' clusters and use K-Means to shift the clusters to the 
 * correct spots. 
 * string = cluster point "1.3-0.2"
 * vector<vector<double>> all data points connected to cluster point
 * */
unordered_map<string, vector<vector<double>>> KMeansCluster::createClusters(int numClusters, int attempts){
    //error checking
    if(numClusters<1){
        string e = "The number of clusters must be greater than 0.";
        throw e;
    }
    if((size_t)numClusters>=data.size()){
        string e = "The number of clusters equals or exceeds the dataset size.";
        throw e;
    }
    if(attempts<1){
        string e = "The number of attempts must be greater than 0.";
        throw e;
    }

    //create the return value
    unordered_map<string, vector<vector<double>>> retVal;

    //We'll keep a double which represents the average distance from a point to a cluster handy, to try and minimize this
    double averageDistance = DBL_MAX;

    //we need to create clusters N number of times
    for(int attempt = 0;attempt<attempts;attempt++){

        //seed the random numbers
        srand(time(NULL));

        //This ensures that the random number generated for 4 data points will have the same index the first two times
        //srand((unsigned int) 400);

        vector<vector<double>> clusterCenters;
        vector<size_t> indecies;

        //reserve the proper space for the clusters
        clusterCenters.reserve(numClusters);

        //need to create numClusters clusters
        for(int i = 0;i<numClusters;i++){
            //variable to check if we need to restart due to duplicate indicies
            bool restartLoop = false;

            //create an index for the new cluster
            size_t index = rand()%data.size();
            //check each existing cluster for duplicates
            for(size_t check = 0;check<indecies.size();check++){
                //if the vectors match, they're the same point
                if(indecies[check]==index){
                    restartLoop = true;
                    break;
                }
            }
            if(restartLoop){
                i--;
                continue;
            }

            //if we got here, the point is not a cluster yet
            clusterCenters.push_back(data[index]);
            indecies.push_back(index);
        }

        //we need somewhere to keep track of the data points, to tell if we continue the process of changing clusters
        vector<vector<double>> points; //by index
        //In outer vector:
            // vector{ Index of cluster,      Distance to cluster }

        //we'll need a vector of this size
        points.reserve(data.size());

        //then fill it with the data assuming the correct cluster is cluster 0;
        for(size_t i = 0;i<data.size();i++){
            //set the default values for the distances between point i and the first cluster center
            //                          P_x       -  Cluster_x      ^2            P_y       - Cluster_y ^2
            double distance = sqrt( pow(data[i][0]-clusterCenters[0][0],2) + pow(data[i][1]-clusterCenters[0][1],2) );
            points.push_back(vector<double>());
            points[i].push_back(0);
            points[i].push_back(distance);
        }

        //This whole thing starting here will be repeated until there are no changes 
        while(true){
            bool somethingHasChanged = false;
            //for each point
            for(size_t i = 0;i<data.size();i++){
                //check each cluster after the 1st to see if it is closer than the first cluster
                for(size_t j = 1;j<clusterCenters.size(); j++){
                    double distance = sqrt( pow(data[i][0]-clusterCenters[j][0],2) + pow(data[i][1] - clusterCenters[j][1],2) );
                    if(distance<points[i][1]){
                        //The point is closer to the second cluster, so modify the cluster center and distance
                        points[i][0] = j;
                        points[i][1] = distance;
                        somethingHasChanged = true;
                    }
                }
            }

            if(!somethingHasChanged){
                break;
            }

            //otherwise move the clusters
            double xpos = 0;
            double ypos = 0;
            double numPoints = 0;
            for(size_t cluster = 0;cluster<clusterCenters.size();cluster++){
                for(size_t point = 0;point<points.size();point++){
                    if(points[point][0] == cluster){
                        xpos+=data[point][0];
                        ypos+=data[point][1];
                        numPoints++;
                    }
                }
                xpos = xpos/numPoints;
                ypos = ypos/numPoints;
                clusterCenters[cluster][0] = xpos;
                clusterCenters[cluster][1] = ypos;

                xpos = 0;
                ypos = 0;
                numPoints = 0;
            }

            //we need to recalculate all the distances between points and their previous cluster now
            for(size_t point = 0;point<points.size();point++){

                //the cluster associated with the current point
                int clustNum = (int)points[point][0];

                //calculate the distance between the point and its cluster
                double distance = sqrt(pow(data[point][0] - clusterCenters[clustNum][0], 2) + pow(data[point][1] - clusterCenters[clustNum][1], 2));
                
                //change the current distance in points to the proper distance
                points[point][1] = distance;
            }
        }

        //we need to check against the current best cluster centers and set them if they are better overall

        //we need a running total to check against the global 'averageDistance'
        double runningTotal = 0;

        //the points array used earlier had each point and a distance to its cluster
        for(size_t point = 0;point<points.size();point++){
            runningTotal += points[point][1];
        }
        runningTotal = runningTotal/(double)points.size();

        //compare runningTotal to averageDistance and replace the current retVal with the new clusters if 
        //the new running Total is less than the previous
        if(runningTotal<averageDistance){
            averageDistance = runningTotal;
            //get rid of the previuos results, so the new pairs can be put in
            retVal.clear();
            //assign the cluster a key and match its points in the value
            for(size_t cluster = 0;cluster<clusterCenters.size();cluster++){
                string key = to_string(clusterCenters[cluster][0]) + "-" + to_string(clusterCenters[cluster][1]);
                vector<vector<double>> value;
                for(size_t point = 0;point<points.size();point++){
                    if(points[point][0] == cluster){
                        value.push_back(data[point]);
                    }
                }
                retVal.emplace(key, value);
            }
        }
    }

    return retVal;
}

/**
 * Debug function to print out the current data points
 * */
void KMeansCluster::printData(){
    cout << "All Data:" << endl;
    for (auto iter = data.begin(); iter != data.end(); iter++)
    {
        for (size_t i = 0; i < data[0].size(); i++)
        {
            cout << (*iter)[i] << " ";
        }
        cout << endl;
    }
    cout << endl;
}