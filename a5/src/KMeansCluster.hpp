#pragma once
#include <vector>
#include <unordered_map>
#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <stdlib.h>
#include <time.h>

using namespace std;

class KMeansCluster{
public:
    KMeansCluster(vector<vector<double>>& kmd);
    unordered_map<string, vector<vector<double>>> createClusters(int numClusters, int attempts);
    
    void printData();
private:
    vector<vector<double>> data;

    void removeDuplicates();
};