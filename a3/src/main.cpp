#include <iostream>
#include <vector>
#include "LinearRegressionModel.hpp"

using namespace std;

void firstTest()
{
    //predicted values to keep code working
    double predVal1, predVal2, predVal3, predVal4 = 0;
    vector<double> unknownInput = {0.0, 4.0};

    //do the base test (should work)
    vector<vector<double>> features1 = {{1.0, 1.0}, {2.0, 2.0}, {3.0, 3.0}};
    vector<double> labels1 = {3.0, 2.0, 1.0};
    try
    {
        LinearRegressionModel model1(features1, labels1, .1);
        predVal1 = model1.predict(unknownInput);
    }
    catch (const char *e) 
    {
        cout << e << endl;
    }

    //do the opposite labels (should still work, also give same answer)
    vector<vector<double>> &features2 = features1;
    vector<double> labels2 = {1.0, 2.0, 3.0};
    try
    {
        LinearRegressionModel model2 = LinearRegressionModel(features2, labels2, .1);
        predVal2 = model2.predict(unknownInput);
    }
    catch (const char *e)
    {
        cout << e << endl;
    }

    //do one that doesn't work
    vector<vector<double>> features3 = {};
    vector<double> labels3 = {};
    try
    {
        LinearRegressionModel model3(features3, labels3, .1);
        predVal3 = model3.predict(unknownInput);
    }
    catch (const char *e)
    {
        cout << e << endl;
    }

    //do one that doesn't work
    vector<vector<double>> features4 = {{1.0, 2.0}, {3.0, 4.0}, {5.0}};
    vector<double> labels4 = {3.0, 2.0, 5.0};
    try
    {
        LinearRegressionModel model4(features4, labels4, .1);
        predVal4 = model4.predict(unknownInput);
    }
    catch (const char *e)
    {
        cout << e << endl;
    }

    cout << "The first predicted value is " << predVal1 << endl;

    cout << "The second predicted value is: " << predVal2 << endl;

    cout << "The third predicted value is: " << predVal3 << endl;

    cout << "The fourth predicted value is: " << predVal4 << endl;
}

void secondTest()
{
    vector<vector<double>> features = {{1.0, 2.0}, {3.0, 3.0}, {4.0, 4.0}};
    vector<double> labels = {3.0, 1.0, 0.0};

    LinearRegressionModel model(features, labels, .1); //2.9

    vector<double> unknownInput = {1.0, 1.0};

    double predictedValue = model.predict(unknownInput);

    cout << "The predicted value is " << predictedValue << endl;
}

void thirdTest()
{
    vector<vector<double>> features = {{6.2}, {2.8}, {8.3}, {1.5}, {2.0}, {3.7}, {4.4}, {9.4}, {6.9}};
    vector<double> labels = {19.0, 9.0, 40.0, 7.0, 20.0, 17.0, 27.0, 44.0, 30.0};

    LinearRegressionModel model(features, labels, .01); //-13.1 with a learning rate of .01

    vector<double> unknownInput = {-4.0};

    double predictedValue = model.predict(unknownInput);

    cout << "The predicted value is " << predictedValue << endl;
}

int main()
{
    //firstTest();
    secondTest();
    thirdTest();
    return 0;
}