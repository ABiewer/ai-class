#pragma once
#include <vector>

using namespace std;

class LinearRegressionModel
{
public:
    LinearRegressionModel(const vector<vector<double>> &features, const vector<double> &labels, double learningRate);
    double predict(const vector<double> &inputFeatures);

private:
    vector<double> weights;
    void gradientDescent(const vector<vector<double>> &xs, const vector<double> &ys, double alpha);
    double costFunction(const vector<double> &x);
    void printWeights();
};
