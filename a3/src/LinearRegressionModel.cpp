#include "LinearRegressionModel.hpp"
#include <vector>
#include <iostream>

using namespace std;

/**
 * Takes in a vector of features {(x_i^0, x_i^1, x_i^2...), ...} and labels (y_i, ...) and a value for alpha
 * and creates a model for the regression line based on a .0001 delta for convergence for each weight.
 * @Throw When the input is invalid, (wrong number of features, wrong )
 * */
LinearRegressionModel::LinearRegressionModel(const vector<vector<double>> &features, const vector<double> &labels, double learningRate)
{
	//some error checking
	if (features.empty() || labels.empty())
	{
		throw "Please provide actual input data, one of your vectors is empty";
	}
	if (features.size() != labels.size())
	{
		throw "Your number of features does not match your number of labels";
	}
	size_t numX = features.front().size();
	for (auto iter = features.begin(); iter != features.end(); iter++)
	{
		if ((*iter).size() != numX)
		{
			throw "Please make sure that each feature has the same number of elements";
		}
	}
<<<<<<< HEAD:Assignment3/src/LinearRegressionModel.cpp
	if(learningRate<=0.0){
=======
	if (learningRate <= 0)
	{
>>>>>>> 0255dab0df4bff59dd3bb834c2a8e14c2a614210:a3/src/LinearRegressionModel.cpp
		throw "Your learning rate cannot be 0 or negative";
	}

	//initialize all the weights to be 0
	weights = vector<double>(numX + 1, 0.0);

	//finish construction
	gradientDescent(features, labels, learningRate);
}

/**
 * Given a set of features, this will predict the value from the weights
 * */
double LinearRegressionModel::predict(const vector<double> &inputFeatures)
{
	double retVal = 0.0;
	auto iter = inputFeatures.begin();
	auto witer = weights.begin();
	//w0 has no associated x value
	retVal += *witer;
	witer++;
	for (; iter != inputFeatures.end(); iter++)
	{
		retVal += *iter * *witer;
		witer++;
	}
	return retVal;
}

/**
 * gradient descent function
 * loop over weights{
 *      w_i = w_i - alpha * 1/n * sum( (h(x's) - y) * x_i)
 * }
 * */
void LinearRegressionModel::gradientDescent(const vector<vector<double>> &xs, const vector<double> &ys, double alpha)
{
	//number of weights
	double n = (double)(weights.size());

outer:
	//deltas determine whether or not we stop (every delta < .0001)
	vector<double> deltas((int)(n), 0.0);
	vector<double> newWeights((int)(n), 0.0);

	//the w0 term is independent of the weights for x
	{
		//find the sum within the delta expression (sum(h-y)*x)
		double sumVal = 0.0;
		//j is the index for a point (x0, x1, x2)
		for (size_t j = 0; j < xs.size(); j++)
		{
			double h = costFunction(xs[j]);
			sumVal += (h - ys[j]); // * (xs[j][0] == 1);
		}
		deltas[0] = -alpha / (double)(xs.size()) * sumVal;
		newWeights[0] = weights[0] + deltas[0];
	}

	//iterate over every weight
	//i is the index for the weight; w1, w2, corresponds to x[point][i]
	for (size_t i = 1; i < deltas.size(); i++)
	{
		//find the sum within the delta expression (sum(h-y)*x)
		double sumVal = 0.0;
		//j is the index for a point (x0, x1, x2)
		for (size_t j = 0; j < xs.size(); j++)
		{
			double h = costFunction(xs[j]);
			sumVal += (h - ys[j]) * xs[j][i - 1];
		}
		deltas[i] = -alpha / (double)(xs.size()) * sumVal;
		newWeights[i] = weights[i] + deltas[i];
	}

	//change the old weights to the new ones, the old weights will be deleted at the end of the loop
	swap(weights, newWeights);

	//debug code
	//printWeights();

	//check each delta to see if we're done yet
	for (size_t i = 0; i < deltas.size(); i++)
	{
		if (deltas[i] > .0001 || deltas[i] < -.0001)
		{
			goto outer;
		}
	}

	//all the |deltas| < .0001, so we are finished.

	return;
}

/**
 * returns the y-value from the weights
 * */
double LinearRegressionModel::costFunction(const vector<double> &x)
{
	auto witer = weights.begin();
	double sum = *witer;
	witer++;
	for (auto iter = x.begin(); iter != x.end(); iter++)
	{
		sum += *iter * *witer;
		witer++;
	}
	return sum;
}

/**
 * Debug code, prints the weights
 * */
void LinearRegressionModel::printWeights()
{
	for (auto iter = weights.begin(); iter != weights.end(); iter++)
	{
		cout << *iter << ",";
	}
	cout << endl;
}