#include <iostream>
#include <iomanip>
#include <algorithm>
#include <ctime>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include "NeuralNetwork.h"

using namespace std;

void readInFiles(const string& filename, vector<vector<vector<double>>>& allData){
	//try to open the file containing the data
	ifstream file;
	file.open(filename);
	if(!file.is_open()){
		throw ("File " + filename + " could not be opened. Exiting..."); //RIP
	}

	//an entry to hold a single game
	vector<vector<double>> singleEntry;

	//a set of data points for labels/inputs
	vector<double> singleInput;
	vector<double> singleLabel;

	//the current line in the file
	string line;
	//for every line in the file,
	while(getline(file, line)){
		//find the line's length and save the proper amount of space in the inputs
		size_t len = line.find(',');
		singleInput.reserve(len);
		//for each character in the tictactoe string,
		for(size_t i = 0;i<len;i++){
			//set it to the correct value (input)
			if(line[i] == 'X'){
				singleInput.push_back(1.0);
			}
			else if(line[i] == 'O'){
				singleInput.push_back(-1.0);
			}
			else{
				singleInput.push_back(0.0);
			}
		}
		singleEntry.push_back(singleInput);
		//now for the labels
		int i = len+2; //index of starting point for labels (11)
		//each label is either X_WIN, O_WIN, or DRAW, so the first letter tells us the result
		if(line[i] == 'X'){
			singleLabel = {1.0,0.0,0.0};
		}
		else if(line[i] == 'O'){
			singleLabel = {0.0,1.0,0.0};
		}
		else{ //DRAW
			singleLabel = {0.0,0.0,1.0};
		}
		singleEntry.push_back(singleLabel);
		//the input and label are both in the single entry, add it to all of the data
		allData.push_back(singleEntry);
	}
	file.close();
}

int main()
{
	srand(unsigned(time(NULL)));
    vector<int> networkArchitecture = { 9, 6, 3 };
	//learning rate
	double alpha = .5;
	//number of training iterations
	double nti = 100;

	string filenameX = "/home/abiewer/Documents/ai-class/Assignment4/xWins.txt";
	string filenameO = "/home/abiewer/Documents/ai-class/Assignment4/oWins.txt";
	string filenameDraw = "/home/abiewer/Documents/ai-class/Assignment4/draws.txt";

	//vector for all of the input data
	vector<vector<vector<double>>> allData;

	//vectors for the test data
	vector<vector<double>> testInputs;
	vector<vector<double>> testLabels;

	//vectors for the training data
	vector<vector<double>> trainInputs;
	vector<vector<double>> trainLabels;

	try{
		// we really want to make sure the draws are all correct
		for(int i = 0;i<10; i++){
			readInFiles(filenameDraw, allData);
		}
		//give the program all the data
		readInFiles(filenameX, allData);
		readInFiles(filenameO, allData);
		
		cout<<"All data size: "<<allData.size()<<endl;
		
		//we need to shuffle the data entries for testing/training
		random_shuffle(allData.begin(),allData.end());

		//use 75% of all data for training
		double trainingProportion = 0.75;
		auto iter = allData.begin();
		for(size_t i = 0;i<allData.size()*trainingProportion;i++){
			trainInputs.push_back((*iter)[0]);
			trainLabels.push_back((*iter)[1]);
			iter++;
		}
		//put the rest of the data in the testing section
		while(iter!=allData.end()){
			testInputs.push_back((*iter)[0]);
			testLabels.push_back((*iter)[1]);
			iter++;
		}

		//make the neural network
		NeuralNetwork nn(networkArchitecture, trainInputs, trainLabels, alpha, nti);

		//now to test the nn...
		auto labelIter = testLabels.begin();
		auto inputIter = testInputs.begin();

		//keep track of the number of incorrect values
		int wrong = 0;
		double globalError = 0.0;

		//for every data entry
		for(;inputIter!=testInputs.end();){
			//value to keep track of the error
			double error = 0.0;

			//predict the results from the nn
			auto results = nn.predict(*inputIter);

			//keep track of which value is the largest
			unsigned int resindex, testindex = 0;
			//and the value of the largest index
			double resmax = -1.0;

			//for each individual label
			for(size_t i = 0;i<results.size();i++){
				//check to see if it is the max value
				if(results[i]>resmax){
					resmax = results[i];
					resindex = i;
				}
				//check to see if the label is the max value
				if((*labelIter)[i]==1.0){
					testindex = i;
				}
			}

			//go through the results and find the error
			cout<<"Results for data entry: "<<inputIter-testInputs.begin()<<endl;
			cout<<"Correct location:"<<testindex<<endl;
			cout<<"results:";
			for(size_t i = 0;i<results.size();i++){
				if(i==testindex)
					error += abs(results[i]-1.0);
				else
					error += abs(results[i]);//-0.0
				cout<<results[i]<<" ";
			}
			cout<<endl;
			cout<<"Labels: "<<(*labelIter)[0]<<"        "<<(*labelIter)[1]<<"        "<<(*labelIter)[2]<<endl<<endl;
			//check if we got the right answer
			if(testindex!=resindex)
				wrong++;
			//get the correct error
			error = error/3.0;
			globalError+=error;

			labelIter++;
			inputIter++;
		}
		globalError = globalError/(double)(testLabels.size()); //average error

		cout<<"The program got "<<wrong<<" predictions wrong out of "<<testLabels.size()<<", and the average error was "<< globalError<<endl;

	}
	catch(const char * e){
		cout<<e<<endl;
	}
    return 0;
}