#pragma once
#include "TicTacToeBoard.h"
#include <vector>
#include <deque>
#include <string>

using namespace std;

class TicTacToeTree
{
public:
	void breadthFirstSearchForOutcome(string boardString, TicTacToeBoard::BOARD_STATE const& requestedState);
	void depthFirstSearchForOutcome(string boardString, TicTacToeBoard::BOARD_STATE const& requestedState);

private:
	struct Node
	{
		TicTacToeBoard* board;
		vector < Node* > children;
		Node* parent;
	};
	void searchHelper(string BorD, Node* root, TicTacToeBoard::BOARD_STATE const& requestedState, int& boardsCreated);
	void createChildren(Node* root, std::vector<Node*>& children, int& boardsCreated);
	std::vector<Node*> createdNodes;
};