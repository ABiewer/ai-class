#include <iostream>
#include <string>
#include "TicTacToeTree.h"

using namespace std;

/**
 * Takes an initial condition, and generates all sequential states using breadth first search until it finds the requested State
 * 
 * Prints an error if the input is invalid
 * */
void TicTacToeTree::breadthFirstSearchForOutcome(string boardString, TicTacToeBoard::BOARD_STATE const &requestedState)
{
	while(!createdNodes.empty()){
		createdNodes.pop_back();
	}
	int boardsCreated = 0;
	TicTacToeBoard *root = nullptr;
	try
	{
		root = new TicTacToeBoard(boardString);
		boardsCreated++;
	}
	catch (char const *exception)
	{
		cout << exception << endl;
		return;
	}
	std::vector<Node *> children;
	Node *rootNode = new Node{root, children, nullptr};
	createdNodes.push_back(rootNode);
	try
	{
		searchHelper("BFS", rootNode, requestedState, boardsCreated);
	}
	catch (char const *exception)
	{
		cout << exception << endl;
		cout << "Exiting..." << endl;
		return;
	}
}

//--
void TicTacToeTree::depthFirstSearchForOutcome(string boardString, TicTacToeBoard::BOARD_STATE const &requestedState)
{
	while(!createdNodes.empty()){
		createdNodes.pop_back();
	}
	int boardsCreated = 0;
	TicTacToeBoard *root = nullptr;
	try
	{
		root = new TicTacToeBoard(boardString);
		boardsCreated++;
	}
	catch (char const *exception)
	{
		cout << exception << endl;
		return;
	}
	std::vector<Node *> children;
	Node *rootNode = new Node{root, children, nullptr};
	createdNodes.push_back(rootNode);
	try
	{
		searchHelper("DFS", rootNode, requestedState, boardsCreated);
	}
	catch (char const *exception)
	{
		cout << exception << endl;
		cout << "Exiting..." << endl;
		return;
	}
}
//--
void TicTacToeTree::searchHelper(string BorD, Node *root, TicTacToeBoard::BOARD_STATE const &requestedState, int &boardsCreated)
{
	std::deque<Node *> frontier;
	frontier.push_back(root);
	bool breadthFirst = false;
	if (BorD == "BFS")
	{
		breadthFirst = true;
	}
	while (!frontier.empty())
	{
		Node *current;
		if (breadthFirst)
		{
			current = frontier.front();
			frontier.pop_front();
		}
		else
		{
			current = frontier.back();
			frontier.pop_back();
		}
		if (current->board->getBoardState() == requestedState)
		{
			if(breadthFirst){
				cout<<"Breadth First Search, ";
			}
			else{
				cout<<"Depth First Search, ";
			}
			if(requestedState == TicTacToeBoard::BOARD_STATE::X_WIN){
				cout<<" X win";
			}
			else if(requestedState == TicTacToeBoard::BOARD_STATE::O_WIN){
				cout<<" O win";
			}
			else{
				cout<<" Draw";
			}
			cout<<endl;

			vector<Node *> printOrder;
			Node *printNode = current;
			int printCount = 1;
			while (printNode != nullptr)
			{
				printOrder.push_back(printNode);
				printNode = printNode->parent;
			}
			while (!printOrder.empty())
			{
				cout<<printCount<<"."<<endl;
				printCount++;
				printOrder.back()->board->printBoard();
				printOrder.pop_back();
			}
			//int boardsDeleted = 0;
			for (auto iter = createdNodes.begin(); iter != createdNodes.end(); iter++)
			{
				//boardsDeleted++;
				delete (*iter)->board;
				delete *iter;
			}
			cout<<"There were "<<boardsCreated<<" boards created."<<endl;
			//cout<<"There were "<<boardsDeleted<<" boards deleted."<<endl; //Matches with boards Created
			return;
		}
		else
		{
			if (current->board->getBoardState() != TicTacToeBoard::BOARD_STATE::INCOMPLETE_GAME)
			{
				//don't create children from the current node, since the game is complete in a non-goal state
				continue;
			}
			if (current->children.empty())
			{
				vector<Node *> toBeChildren;
				createChildren(current, toBeChildren, boardsCreated);
				current->children = toBeChildren;
			}
			for (auto child = current->children.begin(); child != current->children.end(); child++)
			{
				frontier.push_back(*child);
			}
		}
	}
	for (auto iter = createdNodes.begin(); iter != createdNodes.end(); iter++)
	{
		delete (*iter)->board;
		delete *iter;
	}
	std::cout << "There was no solution found. Exiting..." << std::endl;
	return;
}
//--
void TicTacToeTree::createChildren(Node *root, std::vector<Node *> &children, int &boardsCreated)
{
	string parentString = root->board->getBoardString();
	TicTacToeBoard::PLAYER_TURN turn = root->board->getPlayerTurn();
	string replaceLetter;
	if (turn == TicTacToeBoard::PLAYER_TURN::O_TURN)
	{
		replaceLetter = "O";
	}
	else
	{
		replaceLetter = "X";
	}
	int bd = root->board->getBoardDimension();
	for (int i = 0; i < bd; i++)
	{
		for (int j = 0; j < bd; j++)
		{
			if (parentString[i + bd * j] == '-')
			{
				string childString = parentString;
				childString.replace(i + bd * j, 1, replaceLetter);
				TicTacToeBoard *board = new TicTacToeBoard(childString);
				boardsCreated++;
				std::vector<Node *> newChildren;
				Node *child = new Node{board, newChildren, root};
				createdNodes.push_back(child);
				children.push_back(child);
			}
		}
	}
}