#include <iostream>
#include "TicTacToeTree.h"

using namespace std;

void callAllThree(TicTacToeTree& tree, string& boardString){
    tree.breadthFirstSearchForOutcome(boardString, TicTacToeBoard::BOARD_STATE::X_WIN);
    tree.breadthFirstSearchForOutcome(boardString, TicTacToeBoard::BOARD_STATE::O_WIN);
    tree.breadthFirstSearchForOutcome(boardString, TicTacToeBoard::BOARD_STATE::DRAW);

    tree.depthFirstSearchForOutcome(boardString, TicTacToeBoard::BOARD_STATE::X_WIN);
    tree.depthFirstSearchForOutcome(boardString, TicTacToeBoard::BOARD_STATE::O_WIN);
    tree.depthFirstSearchForOutcome(boardString, TicTacToeBoard::BOARD_STATE::DRAW);
}

int main()
{
	TicTacToeTree tree;
    //X goes first, so don't have more O's than X's
    string boardString;

    boardString = "X---O----";
    callAllThree(tree, boardString);

    boardString = "-X-------";
    callAllThree(tree, boardString);

    return 0;
}