#include <iostream>
#include <string>
#include <deque>
#include <cmath>
#include "TicTacToeTree.h"

using namespace std;

TicTacToeTree::TicTacToeTree()
{
}

TicTacToeTree::~TicTacToeTree()
{
}

/**
 * Takes in a string for a board and calculates all of the possible outcomes then returns them in a struct. 
 * */
TicTacToeTree::WinDrawStats TicTacToeTree::depthFirstCountOutcomes(string boardString)
{
	WinDrawStats retVal{0, 0, 0};
	//starting from the current board state given, we create a node holding all of the relevant information about this state
	TicTacToeBoard *root = nullptr;
	try
	{
		root = new TicTacToeBoard(boardString);
	}
	catch (char const *exception)
	{
		//invalid board String
		cout << exception << endl;
		return retVal;
	}
	vector<Node *> children;
	Node *rootNode = new Node{root, children, nullptr};

	//then we set up the depth first search
	deque<Node *> frontier;
	frontier.push_back(rootNode);

	//loop until we run out of options
	int loopcount = 0;
	while (!frontier.empty())
	{
		if (loopcount > 0 && frontier.front() == rootNode)
		{
			frontier.pop_front();
		}
		Node *currentNode = frontier[0];
		boardString = (*currentNode).board->getBoardString();
		frontier.pop_front();
		TicTacToeBoard::BOARD_STATE currentState = (*currentNode).board->getBoardState();
		//if the game is incomplete, we need to create all the subnodes and dfs them
		if (currentState == TicTacToeBoard::BOARD_STATE::INCOMPLETE_GAME)
		{
			//find the current move to replace in the string
			string currentMove;
			if ((*currentNode).board->getPlayerTurn() == TicTacToeBoard::PLAYER_TURN::X_TURN)
			{
				currentMove = "X";
			}
			else
			{
				currentMove = "O";
			}
			//loop over every position in the board
			for (size_t i = 0; i < boardString.length(); i++)
			{
				char currentPos = boardString.at(i);
				//if the move is available, add it to the list of moves to go through.
				if (currentPos == '-')
				{
					string nextString = boardString;

					nextString.replace(i, 1, currentMove);

					TicTacToeBoard *childBoard = new TicTacToeBoard(nextString); //shouldn't throw an error, since we only replace 1 letter
					Node *childNode = new Node{childBoard, vector<Node *>(), currentNode};

					(*currentNode).children.push_back(childNode);

					frontier.push_front(childNode);
				}
				else
				{
					continue;
				}
			}
			loopcount++;
		}
		else
		{
			if (currentState == TicTacToeBoard::BOARD_STATE::X_WIN)
			{
				retVal.numXWins++;
				auto iter = currentNode->parent->children.begin();
				while (*iter != currentNode)
				{
					iter++;
					if (iter == currentNode->parent->children.end())
					{
						cout << "Error with iteration for children" << endl;
					}
				}
				currentNode->parent->children.erase(iter);
			}
			else if (currentState == TicTacToeBoard::BOARD_STATE::O_WIN)
			{
				retVal.numOWins++;
			}
			else
			{
				retVal.numDraws++;
			}
		}
		if ((*currentNode).children.empty())
		{
			Node *parent = currentNode->parent;
			delete currentNode->board;
			delete currentNode;
		}
	}

	return retVal;
}

/**
 * Takes in a string for a board and determines which move is the best move using three heuristics:
 * 1. Win if possible
 * 2. Block a loss
 * 3. Use ratio of wins/losses (possibly with weights for depth) to determine which move to take
 * */
TicTacToeTree::TicTacToePlay TicTacToeTree::nextMoveHeuristic(string boardString)
{
	TicTacToeTree::TicTacToePlay retVal{0, 0, TicTacToeBoard::X_TURN};
	TicTacToeBoard *board;
	try
	{
		board = new TicTacToeBoard(boardString);
		if (board->getBoardState() != TicTacToeBoard::BOARD_STATE::INCOMPLETE_GAME)
		{
			throw "Invalid input string, game is already completed";
		}
	}
	catch (char const *exception)
	{
		cout << exception << endl;
		return retVal;
	}

	TicTacToeBoard::PLAYER_TURN currentTurn = board->getPlayerTurn();

	TicTacToeTree::Helper ptw = playToWin(board, currentTurn);
	if (ptw.heuristic)
	{
		return TicTacToeTree::TicTacToePlay{ptw.row, ptw.col, currentTurn};
	}
	TicTacToeTree::Helper bl = blockLoss(board, currentTurn);
	if (bl.heuristic)
	{
		return TicTacToeTree::TicTacToePlay{bl.row, bl.col, currentTurn};
	}
	TicTacToeTree::Helper other = otherMove(board,currentTurn);
	return TicTacToePlay{other.row,other.col,currentTurn};

}

TicTacToeTree::Helper TicTacToeTree::playToWin(TicTacToeBoard *board, TicTacToeBoard::PLAYER_TURN playerTurn)
{
	for (int row = 0; row < board->getBoardDimension(); row++)
	{
		for (int col = 0; col < board->getBoardDimension(); col++)
		{
			if (board->getSquare(row, col) == TicTacToeBoard::SQUARE_OCCUPANT::EMPTY)
			{
				TicTacToeBoard *newBoard = new TicTacToeBoard{board->getBoardString()};
				TicTacToeBoard::SQUARE_OCCUPANT XorO;
				if (playerTurn == TicTacToeBoard::PLAYER_TURN::X_TURN)
				{
					XorO = TicTacToeBoard::SQUARE_OCCUPANT::X;
				}
				else
				{
					XorO = TicTacToeBoard::SQUARE_OCCUPANT::O;
				}
				newBoard->setSquare(row, col, XorO);
				if (newBoard->getBoardState() != TicTacToeBoard::BOARD_STATE::INCOMPLETE_GAME)
				{
					return TicTacToeTree::Helper{true, row, col};
				}
			}
		}
	}
	return TicTacToeTree::Helper{false, 0, 0};
}

TicTacToeTree::Helper TicTacToeTree::blockLoss(TicTacToeBoard *board, TicTacToeBoard::PLAYER_TURN playerTurn)
{
	for (int row = 0; row < board->getBoardDimension(); row++)
	{
		for (int col = 0; col < board->getBoardDimension(); col++)
		{
			if (board->getSquare(row, col) == TicTacToeBoard::SQUARE_OCCUPANT::EMPTY)
			{
				TicTacToeBoard *newBoard = new TicTacToeBoard{board->getBoardString()};
				TicTacToeBoard::SQUARE_OCCUPANT XorO;
				if (playerTurn == TicTacToeBoard::PLAYER_TURN::X_TURN)
				{
					XorO = TicTacToeBoard::SQUARE_OCCUPANT::O;
				}
				else
				{
					XorO = TicTacToeBoard::SQUARE_OCCUPANT::X;
				}
				newBoard->setSquare(row, col, XorO);
				if (newBoard->getBoardState() != TicTacToeBoard::BOARD_STATE::INCOMPLETE_GAME)
				{
					delete newBoard;
					return TicTacToeTree::Helper{true, row, col};
				}
				delete newBoard;
			}
		}
	}
	return TicTacToeTree::Helper{false, 0, 0};
}

TicTacToeTree::Helper TicTacToeTree::otherMove(TicTacToeBoard *board, TicTacToeBoard::PLAYER_TURN playerTurn){
	int* percents = new int[(int)pow(board->getBoardDimension(),2)];
	std::fill_n(percents, pow(board->getBoardDimension(),2), 0); 
	for(int row = 0;row<board->getBoardDimension();row++){
		for(int col = 0;col<board->getBoardDimension();col++){
			if(board->getSquare(row,col)==TicTacToeBoard::EMPTY){
				string turn;
				if(board->getPlayerTurn()==TicTacToeBoard::X_TURN){
					turn = "X";
				}
				else{
					turn = "O";
				}
				string boardString = board->getBoardString();
				boardString.replace(row*board->getBoardDimension()+col,1,turn);
				WinDrawStats stats = depthFirstCountOutcomes(boardString);
				percents[row*board->getBoardDimension()+col] = stats.numXWins-stats.numOWins;
			}
		}
	}
	int wlratio = 0;
	int location = 0;
	for(int i = 0;i<pow(board->getBoardDimension(),2);i++){
		if(playerTurn == TicTacToeBoard::X_TURN){
			if(percents[i]>wlratio){
				wlratio=percents[i];
				location = i;
			}
		}
		else{
			if(percents[i]<wlratio){
				wlratio=percents[i];
				location = i;
			}
		}
	}
	delete[] percents;

	return Helper{true, location/board->getBoardDimension(), location%board->getBoardDimension()};
}