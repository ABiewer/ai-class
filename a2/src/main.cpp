#include <iostream>
#include "TicTacToeTree.h"

using namespace std;

void play(string boardString);

int main()
{
	/*Code works
    string boardString = "---------";
    TicTacToeTree tree;
    TicTacToeTree::WinDrawStats stats = tree.depthFirstCountOutcomes(boardString);

    int completeGames = stats.numOWins + stats.numXWins + stats.numDraws;

    cout << "Number of O wins: " << stats.numOWins << endl;
    cout << "Number of X wins: " << stats.numXWins << endl;
    cout << "Number of draws: " << stats.numDraws << endl;
    cout << "Total complete games: " << completeGames << endl;
    */

	/*
    Number of X wins: 131184
    Number of O wins: 77904
    Number of draws: 46080
    Total complete games: 255168
    */

	play("X--OO---X");
	play("X--O-O---");
	play("X--OO----");
	play("X-O-X----");
	play("---------");

	return 0;
}

void play(string boardString)
{
	TicTacToeTree tree;
	TicTacToeTree::TicTacToePlay thing = tree.nextMoveHeuristic(boardString);

	string turn;
	if (thing.xOrO == TicTacToeBoard::PLAYER_TURN::X_TURN)
	{
		turn = "X";
	}
	else
	{
		turn = "O";
	}

	cout << "Play at row: " << thing.row << ", col: " << thing.col << " for player " << turn << endl;
}