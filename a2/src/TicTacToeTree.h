#pragma once
#include "TicTacToeBoard.h"
#include <vector>
#include <string>

using namespace std;

class TicTacToeTree
{
public:
	TicTacToeTree();
	~TicTacToeTree();

	struct WinDrawStats
	{
		int numXWins;
		int numOWins;
		int numDraws;
	};
	

	struct TicTacToePlay
	{
		int row;
		int col;
		TicTacToeBoard::PLAYER_TURN xOrO;
	};

	struct Helper
	{
		bool heuristic;
		int row;
		int col;
	};

	WinDrawStats depthFirstCountOutcomes(string boardString);
	TicTacToePlay nextMoveHeuristic(string boardString);
	Helper playToWin(TicTacToeBoard * board, TicTacToeBoard::PLAYER_TURN playerTurn);
	Helper blockLoss(TicTacToeBoard * board, TicTacToeBoard::PLAYER_TURN playerTurn);
	Helper otherMove(TicTacToeBoard * board, TicTacToeBoard::PLAYER_TURN playerTurn);

private:
	struct Node
	{
		TicTacToeBoard* board;
		vector < Node* > children;
		Node* parent;
	};

    Node* root;

    int totalBoards;
	int totalXWins;
	int totalOWins;
	int totalDraws;
};